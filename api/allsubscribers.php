<?php
	include 'env.php';

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);	
	// Check connection
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}

	$conn -> set_charset("utf8");

	$sql = "SELECT * FROM subscribers order by created_at";
	$result = $conn->query($sql);
	$rows = array();

	if ($result->num_rows > 0) {
	  // output data of each row
	  while($row = $result->fetch_assoc()) {
		  $rows[] = $row;
	  }
	  echo json_encode($rows);
	} else {
	  echo "[]";
	}
	$conn->close();
?>