<?php
	include 'env.php';

	$valid_form = true;
	$errors = [];
	$input = json_decode(file_get_contents('php://input'),true); 
	
	//pārbauda , ka ienāk checkbox 1 jeb true
	if (!array_key_exists('checkbox',$input) || $input['checkbox'] != 1){
		$errors += ["checkbox" => "You must accept the terms and conditions"];
		$valid_form = false;
	}
	
	//pārbauda ka ienāk e-pasts, tas ir unikāls un ka tas atbilst prasībām
	if (!array_key_exists('email',$input) || strlen($input['email']) == 0) {
		$errors += ["email" => "Email address is required"];
		$valid_form = false;
	}
	
	elseif(!preg_match('/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/',$input['email'])) {
		$errors += ["email" => "Please provide a valid e-mail address"];
		$valid_form = false;
	}
	
	elseif(preg_match('/\.co$/i', $input['email'])){
		$errors += ["email" => "We are not accepting subscriptions from Colombia emails"];
		$valid_form = false;
	}

	// Create connection
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
	  die("Connection failed: " . $conn->connect_error);
	}

	if($valid_form){
		$email_provider = explode("@", $input['email']);
		$sql = "INSERT INTO subscribers (email, email_provider)
		VALUES ('".$input['email']."', '".end($email_provider)."')";

		if ($conn->query($sql) !== TRUE) {
		  $errors += ["Error" => $sql.$conn->error];
		}		
	}
	

	$conn->close();
	
	
	echo json_encode($errors);
?>
