CREATE TABLE subscribers (
  id int UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  email varchar(191) NOT NULL,
  email_provider varchar(191) NOT NULL,
  created_at timestamp DEFAULT NOW()
) COLLATE=utf8mb4_unicode_ci ;

