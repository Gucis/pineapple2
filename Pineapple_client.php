<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="css/SS_to_NL.css">  
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/SS_to_NL_media.css">
	<script src="js/vue.js"></script>
	<script src="js/vue-router.js"></script>
	<script src="js/axios.js"></script>
</head>


<body>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
   
    <div id="container_main">
        <div id="main_menu">
            <img id="pineapple" src="Task 1/Pineapple/logo_pineapple.svg">
            <img id="pineapple-1" src="Task 1/Pineapple/logo_pineapple-1.svg">
            
            <ul>
                <li id="About"><a href="#">About</a></li>
                <li id="Howitworks"><a href="#">How it works</a></li> 
                <li id="Contact"><a href="#">Contact</a></li>   
            
            </ul>
        </div>
        
        <div id="for_media">
            <div id="app">
				<div id="subscribe_box">

				
				   <router-view> </router-view>

					<hr>

							<div class="social_media">
								<span class="dot facebook"><span class="icon-ic_facebook sm_icon"></span></span>
								<span class="dot instagram"><span class="icon-ic-instagram sm_icon"></span></span>
								<span class="dot twitter"><span class="icon-ic_twitter sm_icon"></span></span>
								<span class="dot youtube"><span class="icon-ic-youtube sm_icon"></span></span>
							</div>
				</div>
			</div>
            <script src="js/app.js" type="text/javascript"></script>
        </div>




    </div>
   
    <div id="Pbg">
    </div>

</body>
</html>