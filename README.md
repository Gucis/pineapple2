# Pineapple

Pineapple email subscription web page.

## Installation

Clone repo

```bash
git clone git@bitbucket.org:Gucis/pineapple2.git
```

Enter project folder

```bash
cd pineapple2
```




create database for emails

create api/env.php file from api/env.example.php

specify database location, user, password in env.php file

generate database tables using script in CreateDBtable_subscribers.sql

## Usage

start database server

start php server

Pineapple subscription page:
http://localhost

Table containing all subscribers:
http://localhost/allsubscribers.php
