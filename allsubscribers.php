<html>
    <head>
        <link rel="stylesheet" href="css/SS_to_NL.css"> 
        <link rel="stylesheet" href="css/style.css">
        <link href="css/app.css" type="text/css" rel="stylesheet" />
		<script src="js/vue.js"></script>
		<script src="js/vue-router.js"></script>
		<script src="js/axios.js"></script>
    </head>
    <body>
            <div id="app">
				<router-view> </router-view>
			</div>
            <script src="js/app.js" type="text/javascript"></script>
        
    </body>
</html>
