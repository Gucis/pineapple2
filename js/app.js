

const CreateSubscriber = { template: `<div id="newsletter_box">

        <h1>Subscribe to newsletter</h1>
        <h2>Subscribe to our newsletter and get 10% discount on pineapple glasses.</h2>
        
                    <form @submit.prevent="addSubscriber">
                        <div class="container_input">
                            <input type="email" id="email" name="email" v-model="subscriber.email" v-on:input="validateForm" @blur="startValidate" placeholder="Type your email address here...">
                            <span class="icon-ic_arrow" id="arrow" :disabled="!validateForm()" v-on:click="addSubscriber"></span>
                        </div>

                        <span id="errors">{{errors}}</span>
                        
                        <label class="container_terms">I agree to <a href="#">terms of service</a>
                            <input type="checkbox" name="checkbox" v-model="subscriber.checkbox" @change="validateForm">
                            <span class="checkmark icon-ic_checkmark"></span>
                        </label>
                    </form>


    </div>`,
	
	data() {
            return {
                subscriber: {},
                validate: false,
                errors:null
            }
        },
        methods: {
            validateForm() {
                this.errors = null;
                var retrun_value = true;

                if(this.validate){
                    if (typeof this.subscriber.checkbox === 'undefined' || this.subscriber.checkbox == false) {
                        this.errors = 'You must accept the terms and conditions';
                        
                        retrun_value = false;
                    }
                    else{
                        //checkbox korekts
                    }

                    
                    if (typeof this.subscriber.email === 'undefined' || this.subscriber.email.length == 0) {
                        this.errors = 'Email address is required';
                        retrun_value = false;
                    }

                    else if (!this.validEmail(this.subscriber.email)){
                        this.errors = 'Please provide a valid e-mail address';
                        retrun_value = false;
                    }

                    else if (this.colombiaEmail(this.subscriber.email)){
                        this.errors = 'We are not accepting subscriptions from Colombia emails';
                        retrun_value = false;
                    }
                    else{
                        //epasts korekts
                    }
                }
                else{
                    retrun_value = false;
                }
                return retrun_value;
            },

            startValidate() {
                this.validate = true;
                this.validateForm();
            },

            addSubscriber() {
            if(this.validateForm()){
                axios
                    .post('http://localhost/api/createsubscriber.php', this.subscriber)
                    .then(response => (
                        this.parseError(response.data)
                    ))
                    .catch(err => console.log(err))
                    .finally(() => this.loading = false)
                }
            },

            parseError(errors){
                if(Object.keys(errors).length === 0){
                    this.$router.push({ name: 'thankyou' });
                }else{
                    alert(Object.keys(errors));
                }
            },

            validEmail: function(email) { 
              var re=/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
              return re.test(email);
            },

            colombiaEmail: function(email) {
                var re=/\.co$/;
                return re.test(email);
            }
        }
};


const ThankYou = { template: `<div> 
		<img id="cup" src="Task 1/Icons/Union.svg">
        <h1>Thanks for subscribing!</h1>
             <h3>You have successfully subscribed to our email listing. Check your email for the discount code.</h3> 
	</div>` 
};


const AllSubscribers = { template: `<div> 
		<h2 class="text-center">Subscribers List</h2>
        <div class="btn-group" role="group">
        <button type="button" class="btn btn-outline-primary" v-for="provider in providers" :key="provider" @click="showProvider = provider"> {{provider}} </button>
        </div>
        <h4>{{showProvider}}</h4>
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th @click="sortMethod = 'date'">Date</th>
                <th @click="sortMethod= 'alphabetically'">E-mail</th>
                <th>Provider</th>
                <th><!--Actions--></th>
            </tr>
            </thead>
            <tbody>
            <tr v-for="subscriber in displayedSubscribers" :key="subscriber.id">
                <td>{{ subscriber.id }}</td>
                <td>{{ subscriber.created_at }}</td>
                <td>{{ subscriber.email }}</td>
                <td>{{ subscriber.email_provider }}</td>
                <td>
                    <div class="btn-group" role="group">
                        <!-- <router-link :to="{name: 'edit', params: { id: subscriber.id }}" class="btn btn-success">Edit</router-link> -->
                        <button class="btn btn-danger" @click="deleteSubscriber(subscriber.id)">Delete</button>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>

        <nav aria-label="Page navigation example">
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-primary" v-if="page != 1" @click="page--"> Previous </button>
                <button type="button" class="btn btn-outline-primary" v-for="pageNumber in pages.slice(page-1, page+5)" :key="pageNumber" @click="page = pageNumber"> {{pageNumber}} </button>
                <button type="button" @click="page++" v-if="page < pages.length" class="btn btn-primary"> Next </button>
            </div>
		</nav>
	</div>` ,
	
	data() {
            return {
                subscribers: [],
                page: 1,
                perPage: 10,
                pages: [],	
                sortMethod:'date',
                providers: [],
                showProvider:'All',
            }
        },
        created() {
            axios
                .get('http://localhost/api/allsubscribers.php')
                .then(response => {
                    this.subscribers = response.data;
                    this.filterProviders(this.subscribers);
                });
        },
        methods: {
            deleteSubscriber(id) {
                axios
                    .post('http://localhost/api/deletesubscriber.php',id)
                    .then(response => {
                        let i = this.subscribers.map(data => data.id).indexOf(id);
                        this.subscribers.splice(i, 1)
                    });
            },

            setPages (subscriber_count) {
                let numberOfPages = Math.ceil(subscriber_count / this.perPage);
                this.pages.splice(0);
                for (let index = 1; index <= numberOfPages; index++) {
                    this.pages.push(index);
		    	}
    		},
            paginate (subscribers) {
                this.setPages (subscribers.length);
			    let page = this.page;
			    let perPage = this.perPage;
                let from = (page * perPage) - perPage;
                let to = (page * perPage);
                return  subscribers.slice(from, to);
		    },
            sortSubscribers(subscribers) {
                if (this.sortMethod == 'alphabetically') {
                return subscribers.sort((a,b) => {
                        let fa = a.email.toLowerCase(), fb = b.email.toLowerCase();
                        if (fa < fb) {
                            return -1
                        }
                        if (fa > fb) {
                            return 1
                        }
                        return 0
                    });
                } else if (this.sortMethod == 'date'){
                return subscribers.sort((a,b) => {
                    return new Date(a.created_at) - new Date(b.created_at)
                });
                }else{
                    return subscribers;
                }
            },

            filterProviders(subscribers){
                let providers = subscribers.map(function(s){return s.email_provider});
                this.providers = [ ...new Set(providers)];
                this.providers.push('All');
            },
            filterByProviders(subscribers){
                if(this.showProvider == 'All'){
                    return subscribers;
                }else{
                    return subscribers.filter((s) => {
                        return (s.email_provider == this.showProvider)
                        });
                }
            }
        },

        computed: {
		    displayedSubscribers () {
			    return this.paginate(this.sortSubscribers(this.filterByProviders(this.subscribers)));
		    }
	    },

        filters: {
            trimWords(value){
                return value.split(" ").splice(0,20).join(" ") + '...';
            }
        }
};


 
  const routes = [
    {
        name: 'home',
        path: '/',
        component: CreateSubscriber
    },
    {
        name: 'thankyou',
        path: '/thankyou.php',
        component: ThankYou
    },
    {
        name: 'allsubscribers',
        path: '/allsubscribers.php',
        component: AllSubscribers
    }
];

const router = new VueRouter({
 mode: 'history',
 routes: routes
 });
 
 const app = new Vue({
 router: router
 }).$mount('#app');